<?php

abstract class Draw {
	protected abstract function getRectangle(); // return [[minX, minY], [maxX, maxY]]
	protected abstract function getChar(array $pos); // return the character at ($x, $y)
	protected abstract function spawnPoint($direct = 'r');

	private static $objects = [];
	public static function draw() {
		$min = [null, null];
		$max = [null, null];
		// find min max of all objects
		foreach (self::$objects as $object) {
			list($oMin, $oMax) = $object->getRectangle();
			if ($min[0] === null || $oMin[0] < $min[0]) $min[0] = $oMin[0];
			if ($min[1] === null || $oMin[1] < $min[1]) $min[1] = $oMin[1];
			if ($max[0] === null || $oMax[0] > $max[0]) $max[0] = $oMax[0];
			if ($max[1] === null || $oMax[1] > $max[1]) $max[1] = $oMax[1];
		}
		for ($y=$min[1]; $y<=$max[1]; $y++) {
			$text = '';
			for ($x=$min[0]; $x<=$max[0]; $x++) {
				$char = ' ';
				foreach (self::$objects as $object) {
					$oChar = $object->getChar([$x,$y]);
					if ($oChar != '' && $oChar != ' ') $char = $oChar;
				}
				$text .= $char;
			}
			echo rtrim($text)."\n";
		}
	}

	public static function initText($text, $style='sqr', $pos=[0,0]) {
		$text = new DrawText($pos, $text, $style);
		self::$objects[] = $text;
		return $text;
	}

	public function arrow($direct='r', $len=5) {
		$endChar = '+';
		switch ($direct[0]) {
		case 'r':
			$endChar = '>';
			break;
		case 'l':
			$endChar = '<';
			break;
		case 'u':
			$endChar = '^';
			break;
		case 'd':
			$endChar = 'v';
			break;
		}
		$arrow = new DrawArrow($this->spawnPoint($direct), $len, $direct, '+', $endChar);
		self::$objects[] = $arrow;
		return $arrow;
	}
	public function text($text, $direct='r', $style='sqr') {
		$spawn = $this->spawnPoint($direct);
		$texts = explode("\n", $text);
		$maxLen = 0;
		foreach ($texts as $line) {
			if (strlen($line)>$maxLen)
				$maxLen = strlen($line);
		}
		switch ($direct[0]) {
		case 'r':
			$pos = [$spawn[0], $spawn[1]-1];
			break;
		case 'l':
			$pos = [$spawn[0]-$maxLen-1, $spawn[1]-1];
			break;
		case 'u':
			$pos = [$spawn[0]-1, $spawn[1]-count($texts)-1];
			break;
		case 'd':
			$pos = [$spawn[0]-1, $spawn[1]];
			break;
		}
		$dText = new DrawText($pos, $text, $style);
		self::$objects[] = $dText;
		return $dText;
	}
	public function dot($direct='r', $char='+') {
		$spawn = $this->spawnPoint($direct);
		$dot = new DrawDot($spawn, $char);
		self::$objects[] = $dot;
		return $dot;
	}
	public function arrowText($text, $direct='r', $len=5, $style='sqr') {
		return $this->arrow($direct, $len)->text($text, $direct, $style);
	}
	public function arrowDot($char='+', $direct='r', $len=5) {
		return $this->arrow($direct, $len)->dot($direct, $char);
	}
}

class DrawText extends Draw {
	private $pos, $texts, $maxLen, $style;
	public function __construct(array $pos, $text, $style='sqr') {
		$this->pos = $pos;
		$this->style = $style;
		$this->texts = explode("\n", $text);
		$this->maxLen = 0;
		foreach ($this->texts as $line) {
			if (strlen($line) > $this->maxLen)
				$this->maxLen = strlen($line);
		}
	}
	protected function getRectangle() {
		return [$this->pos, [$this->pos[0]+$this->maxLen+1, $this->pos[1]+count($this->texts)+1]];
	}
	protected function getChar(array $pos) {
		list($min, $max) = $this->getRectangle();
		if ($pos[0] >= $min[0] && $pos[0] <= $max[0] && $pos[1] >= $min[1] && $pos[1] <= $max[1] ) {
			if ($pos[0] == $min[0] && $pos[1] == $min[1]) {
				return $this->style=='rnd' ? '/' : ($this->style=='sqr'?'+':' ');
			} else if ($pos[0] == $min[0] && $pos[1] == $max[1]) {
				return $this->style=='rnd' ? "\\" : ($this->style=='sqr'?'+':' ');
			} else if ($pos[0] == $max[0] && $pos[1] == $min[1]) {
				return $this->style=='rnd' ? "\\" : ($this->style=='sqr'?'+':' ');
			} else if ($pos[0] == $max[0] && $pos[1] == $max[1]) {
				return $this->style=='rnd' ? '/' : ($this->style=='sqr'?'+':' ');
			} else if ($pos[0] == $min[0] || $pos[0] == $max[0]) {
				return $this->style=='rnd' || $this->style=='sqr' ? '|' : ' ';
			} else if ($pos[1] == $min[1] || $pos[1] == $max[1]) {
				return $this->style=='rnd' || $this->style=='sqr' ? '-' : ' ';
			}
			$strIdx = $pos[1]-$min[1]-1;
			$charIdx = $pos[0]-$min[0]-1;
			if ($charIdx < strlen($this->texts[$strIdx])) {
				return $this->texts[$strIdx][$charIdx];
			}
		}
		return ' ';
	}
	protected function spawnPoint($direct = 'r') {
		list($baseMin, $baseMax) = $this->getRectangle();
		switch ($direct[0]) {
		case 'l':
			return [$baseMin[0], $baseMin[1]+1];
		case 'd':
			return [$baseMin[0]+1, $baseMax[1]];
		case 'u':
			return [$baseMin[0]+1, $baseMin[1]];
		case 'r':
		default:
			return [$baseMax[0], $baseMin[1]+1];
		}
	}
	public function withPos($pos) { // damn
		return new DrawText($pos, implode("\n", $this->texts), $this->style);
	}
}

class DrawArrow extends Draw {
	private $start, $end, $startChar, $endChar, $lineChar;
	private $lineCond, $min, $max;
	private $direct; // the first char of direction to calculate spawn point
	public function __construct(array $start, int $len, string $direct='r', $startChar='+', $endChar='+') {
		$this->start = $start;
		$this->startChar = $startChar;
		$this->endChar = $endChar;
		$this->direct = $direct[0];
		switch ($direct) {
		case 'r':
			$this->end = [$start[0]+$len, $start[1]];
			$this->lineChar = '-';
			$this->lineCond = function($pos) use ($start) { return $pos[1] == $start[1]; };
			$this->min = $start;
			$this->max = $this->end;
			break;
		case 'l':
			$this->end = [$start[0]-$len, $start[1]];
			$this->lineChar = '-';
			$this->lineCond = function($pos) use ($start) { return $pos[1] == $start[1]; };
			$this->min = $this->end;
			$this->max = $start;
			break;
		case 'u':
			$this->end = [$start[0], $start[1]-$len];
			$this->lineChar = '|';
			$this->lineCond = function($pos) use ($start) { return $pos[0] == $start[0]; };
			$this->min = $this->end;
			$this->max = $start;
			break;
		case 'd':
			$this->end = [$start[0], $start[1]+$len];
			$this->lineChar = '|';
			$this->lineCond = function($pos) use ($start) { return $pos[0] == $start[0]; };
			$this->min = $start;
			$this->max = $this->end;
			break;
		case 'ru':
		case 'ur':
			$this->end = [$start[0]+$len, $start[1]-$len];
			$this->lineChar = '/';
			$this->lineCond = function($pos) use ($start) { return $pos[0] - $start[0] == $start[1] - $pos[1]; };
			$this->min = [$start[0], $this->end[1]];
			$this->max = [$this->end[0], $start[1]];
			break;
		case 'rd':
		case 'dr':
			$this->end = [$start[0]+$len, $start[1]+$len];
			$this->lineChar = "\\";
			$this->lineCond = function($pos) use ($start) { return $pos[0] - $start[0] == $pos[1] - $start[1]; };
			$this->min = $start;
			$this->max = $this->end;
			break;
		case 'lu':
		case 'ul':
			$this->end = [$start[0]-$len, $start[1]-$len];
			$this->lineChar = "\\";
			$this->lineCond = function($pos) use ($start) { return $pos[0] - $start[0] == $pos[1] - $start[1]; };
			$this->min = $this->end;
			$this->max = $start;
			break;
		case 'ld':
		case 'dl':
			$this->end = [$start[0]-$len, $start[1]+$len];
			$this->lineChar = '/';
			$this->lineCond = function($pos) use ($start) { return $pos[0] - $start[0] == $start[1] - $pos[1]; };
			$this->min = [$this->end[0], $start[1]];
			$this->max = [$start[0], $this->end[1]];
			break;
		}
	}
	protected function getRectangle() {
		return [$this->min, $this->max];
	}
	protected function getChar(array $pos) {
		if ($pos[0] == $this->start[0] && $pos[1] == $this->start[1])
			return $this->startChar;
		if ($pos[0] == $this->end[0] && $pos[1] == $this->end[1])
			return $this->endChar;
		if ($pos[0] >= $this->min[0] && $pos[0] <= $this->max[0]
			&& $pos[1] >= $this->min[1] && $pos[1] <= $this->max[1]
			&& ($this->lineCond)($pos))
			return $this->lineChar;
		return ' ';
	}
	protected function spawnPoint($direct = 'r') {
		switch ($this->direct) {
		case 'r':
			return [$this->end[0]+1, $this->end[1]];
		case 'l':
			return [$this->end[0]-1, $this->end[1]];
		case 'd':
			return [$this->end[0], $this->end[1]+1];
		case 'u':
			return [$this->end[0], $this->end[1]-1];
		}
	}
}

class DrawDot extends Draw {
	private $pos, $char;
	public function __construct(array $pos, $char='+') {
		$this->pos = $pos;
		$this->char = $char;
	}
	protected function getRectangle() {
		return [$this->pos, $this->pos];
	}
	protected function getChar(array $pos) {
		if ($pos[0] == $this->pos[0] && $pos[1] == $this->pos[1])
			return $this->char;
		return ' ';
	}
	protected function spawnPoint($direct = 'r') {
		return $this->pos;
	}
}

function decodeString($str) {
	return str_replace(['*A*', '*N*', '*OP*', '*CP*', '*OB*', '*CB*'], ['*', "\n", '(', ')', '[', ']'], $str);
}

if (count($argv) >= 3 && $argv[1] == 'of' && $argv[2]) {
	$lines = file($argv[2], FILE_SKIP_EMPTY_LINES);
	if ($lines && count($lines)>0) {
		$nodes = [];
		foreach ($lines as $line) {
			preg_match('/^\s*([a-zA-Z]+)(([\(\[])([^\)\]]*)[\)\]])?\s*(-([udlr]?[udlr]?)(\d*)->\s*([a-zA-Z]+)(([\(\[])([^\)\]]*)[\)\]])?)?\s*$/', $line, $matches);
			if (!$matches)
				echo "[ERR]:$line\n";
			$nodeName = $matches[1];
			$nodeText = isset($matches[4]) && $matches[4] ? $matches[4] : $nodeName;
			$nodeStyle = isset($matches[3]) && $matches[3] == '(' ? 'rnd' : 'sqr';
			if (!isset($nodes[$nodeName]) || !$nodes[$nodeName]) {
				$nodes[$nodeName] = Draw::initText(decodeString($nodeText), $nodeStyle);
			}
			$otherName = isset($matches[8]) ? $matches[8] : false;
			$otherText = isset($matches[11]) ? $matches[11] : $otherName;
			$otherStyle = isset($matches[10]) && $matches[10] == '(' ? 'rnd' : 'sqr';
			$arrowDirect = isset($matches[6]) && $matches[6] ? $matches[6] : 'r';
			$arrowLen = isset($matches[7]) && $matches[7] ? $matches[7] : '5';
			if ($otherName) {
				if ($otherText == '*DOT*')
					$nodes[$otherName] = ($nodes[$nodeName])->arrow($arrowDirect, $arrowLen);
				else
					$nodes[$otherName] = ($nodes[$nodeName])->arrowText(decodeString($otherText), $arrowDirect, $arrowLen, $otherStyle);
			}
		}
		Draw::draw();
	}
}
