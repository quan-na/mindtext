(require 'org)
(require 'ob)

(defun org-babel-execute:mindtext (body params)
  "Execute a block of MindText code with org-babel."
  (let ((in-file (org-babel-temp-file "mt" ".mindtext"))
        )
    (with-temp-file in-file
      (insert body))
    (org-babel-eval
     (format "php -f ~/Apps/mindtext/mindtext2.php of %s"
             (org-babel-process-file-name in-file))
     "")))

(provide 'ob-mindtext)
