<?php
abstract class DrawObject {
	public abstract function getRectangle(); // return [[minX, minY], [maxX, maxY]]
	public abstract function getChar(array $pos); // return the character at ($x, $y)
}

class DrawBox extends DrawObject{
	private $pos, $texts, $maxLen, $isRound;
	public function __construct(array $pos, $text, $isRound=false) {
		$this->pos = $pos;
		$this->isRound = $isRound;
		$this->texts = explode("\n", $text);
		$this->maxLen = 0;
		foreach ($this->texts as $line) {
			if (strlen($line) > $this->maxLen)
				$this->maxLen = strlen($line);
		}
	}
	public function getRectangle() {
		return [$this->pos, [$this->pos[0]+$this->maxLen+1, $this->pos[1]+count($this->texts)+1]];
	}
	public function getChar(array $pos) {
		list($min, $max) = $this->getRectangle();
		if ($pos[0] >= $min[0] && $pos[0] <= $max[0] && $pos[1] >= $min[1] && $pos[1] <= $max[1] ) {
			if ($pos[0] == $min[0] && $pos[1] == $min[1]) {
				return $this->isRound ? '/' : '+';
			} else if ($pos[0] == $min[0] && $pos[1] == $max[1]) {
				return $this->isRound ? "\\" : '+';
			} else if ($pos[0] == $max[0] && $pos[1] == $min[1]) {
				return $this->isRound ? "\\" : '+';
			} else if ($pos[0] == $max[0] && $pos[1] == $max[1]) {
				return $this->isRound ? '/' : '+';
			} else if ($pos[0] == $min[0] || $pos[0] == $max[0]) {
				return '|';
			} else if ($pos[1] == $min[1] || $pos[1] == $max[1]) {
				return '-';
			}
			$strIdx = $pos[1]-$min[1]-1;
			$charIdx = $pos[0]-$min[0]-1;
			if ($charIdx < strlen($this->texts[$strIdx])) {
				return $this->texts[$strIdx][$charIdx];
			}
		}
		return ' ';
	}
	public function setPosition(array $pos) {
		$this->pos = $pos;
	}
}

class DrawLine extends DrawObject {
	private $start, $end, $startChar, $endChar, $lineChar;
	private $lineCond, $min, $max;
	public function __construct(array $start, int $len, string $direct='r', $startChar='+', $endChar='+') {
		$this->start = $start;
		$this->startChar = $startChar;
		$this->endChar = $endChar;
		switch ($direct) {
		case 'r':
		case 'right':
			$this->end = [$start[0]+$len, $start[1]];
			$this->lineChar = '-';
			$this->lineCond = function($pos) use ($start) { return $pos[1] == $start[1]; };
			$this->min = $start;
			$this->max = $this->end;
			break;
		case 'l':
		case 'left':
			$this->end = [$start[0]-$len, $start[1]];
			$this->lineChar = '-';
			$this->lineCond = function($pos) use ($start) { return $pos[1] == $start[1]; };
			$this->min = $this->end;
			$this->max = $start;
			break;
		case 'u':
		case 'up':
			$this->end = [$start[0], $start[1]-$len];
			$this->lineChar = '|';
			$this->lineCond = function($pos) use ($start) { return $pos[0] == $start[0]; };
			$this->min = $this->end;
			$this->max = $start;
			break;
		case 'd':
		case 'down':
			$this->end = [$start[0], $start[1]+$len];
			$this->lineChar = '|';
			$this->lineCond = function($pos) use ($start) { return $pos[0] == $start[0]; };
			$this->min = $start;
			$this->max = $this->end;
			break;
		case 'r-u':
		case 'u-r':
		case 'right-up':
		case 'up-right':
			$this->end = [$start[0]+$len, $start[1]-$len];
			$this->lineChar = '/';
			$this->lineCond = function($pos) use ($start) { return $pos[0] - $start[0] == $start[1] - $pos[1]; };
			$this->min = [$start[0], $this->end[1]];
			$this->max = [$this->end[0], $start[1]];
			break;
		case 'r-d':
		case 'd-r':
		case 'right-down':
		case 'down-right':
			$this->end = [$start[0]+$len, $start[1]+$len];
			$this->lineChar = "\\";
			$this->lineCond = function($pos) use ($start) { return $pos[0] - $start[0] == $pos[1] - $start[1]; };
			$this->min = $start;
			$this->max = $this->end;
			break;
		case 'l-u':
		case 'u-l':
		case 'left-up':
		case 'up-left':
			$this->end = [$start[0]-$len, $start[1]-$len];
			$this->lineChar = "\\";
			$this->lineCond = function($pos) use ($start) { return $pos[0] - $start[0] == $pos[1] - $start[1]; };
			$this->min = $this->end;
			$this->max = $start;
			break;
		case 'l-d':
		case 'd-l':
		case 'left-down':
		case 'down-left':
			$this->end = [$start[0]-$len, $start[1]+$len];
			$this->lineChar = '/';
			$this->lineCond = function($pos) use ($start) { return $pos[0] - $start[0] == $start[1] - $pos[1]; };
			$this->min = [$this->end[0], $start[1]];
			$this->max = [$start[0], $this->end[1]];
			break;
		}
	}
	public function getRectangle() {
		return [$this->min, $this->max];
	}
	public function getChar(array $pos) {
		if ($pos[0] == $this->start[0] && $pos[1] == $this->start[1])
			return $this->startChar;
		if ($pos[0] == $this->end[0] && $pos[1] == $this->end[1])
			return $this->endChar;
		if ($pos[0] >= $this->min[0] && $pos[0] <= $this->max[0]
			&& $pos[1] >= $this->min[1] && $pos[1] <= $this->max[1]
			&& ($this->lineCond)($pos))
			return $this->lineChar;
		return ' ';
	}
}

class Diagram {
	private $objects = [];
	public function add(DrawObject $object) {
		$this->objects[] = $object;
		return $this;
	}
	public function draw() {
		$min = [null, null];
		$max = [null, null];
		// find min max of all objects
		foreach ($this->objects as $object) {
			list($oMin, $oMax) = $object->getRectangle();
			if ($min[0] === null || $oMin[0] < $min[0]) $min[0] = $oMin[0];
			if ($min[1] === null || $oMin[1] < $min[1]) $min[1] = $oMin[1];
			if ($max[0] === null || $oMax[0] > $max[0]) $max[0] = $oMax[0];
			if ($max[1] === null || $oMax[1] > $max[1]) $max[1] = $oMax[1];
		}
		for ($y=$min[1]; $y<=$max[1]; $y++) {
			$text = '';
			for ($x=$min[0]; $x<=$max[0]; $x++) {
				$char = ' ';
				foreach ($this->objects as $object) {
					$oChar = $object->getChar([$x,$y]);
					if ($oChar != '' && $oChar != ' ') $char = $oChar;
				}
				$text .= $char;
			}
			echo rtrim($text)."\n";
		}
	}
	public static function ins() {
		return new Diagram();
	}
}

class Draw {
	public static function text($pos, $text=false, $isRound=false) {
		if (is_string($pos)) { // first param is string: create box at (0, 0)
			$isRound = $text;
			$text = $pos;
			$pos = [0, 0];
		}
		return new DrawBox($pos, $text, $isRound);
	}
	public static function arrow(array $start, $len, $direct) {
		$endChar = '+';
		switch ($direct[0]) {
		case 'r':
			$endChar = '>';
			break;
		case 'l':
			$endChar = '<';
			break;
		case 'u':
			$endChar = '^';
			break;
		case 'd':
			$endChar = 'v';
			break;
		}
		return new DrawLine($start, $len, $direct, '+', $endChar);
	}
	public static function connect(DrawObject $base, DrawBox $float, string $direct='r', $len=5) {
		list($baseMin, $baseMax) = $base->getRectangle();
		list($floatMin, $floatMax) = $float->getRectangle();
		switch ($direct[0]) {
		case 'r':
			$arrow = self::arrow([$baseMax[0], $baseMin[1]+1], $len, $direct);
			list($arrowMin, $arrowMax) = $arrow->getRectangle();
			switch ($direct) {
			case 'r':
				$float->setPosition([$arrowMax[0]+1, $arrowMax[1]-1]);
				break;
			case 'r-u':
				$float->setPosition([$arrowMax[0]+1, $arrowMin[1]-1]);
				break;
			case 'r-d':
				$float->setPosition([$arrowMax[0]+1, $arrowMax[1]-1]);
				break;
			}
			return $arrow;
			break;
		case 'l':
			$arrow = self::arrow([$baseMin[0], $baseMin[1]+1], $len, $direct);
			list($arrowMin, $arrowMax) = $arrow->getRectangle();
			switch ($direct) {
			case 'l':
				$float->setPosition([$arrowMin[0]-1-($floatMax[0]-$floatMin[0]), $arrowMax[1]-1]);
				break;
			case 'l-u':
				$float->setPosition([$arrowMin[0]-1-($floatMax[0]-$floatMin[0]), $arrowMin[1]-1]);
				break;
			case 'l-d':
				$float->setPosition([$arrowMin[0]-1-($floatMax[0]-$floatMin[0]), $arrowMax[1]-1]);
				break;
			}
			return $arrow;
			break;
		case 'd':
			$arrow = self::arrow([$baseMin[0]+1, $baseMax[1]], $len, $direct);
			list($arrowMin, $arrowMax) = $arrow->getRectangle();
			switch ($direct) {
			case 'd':
				$float->setPosition([$arrowMin[0]-1, $arrowMax[1]+1]);
				break;
			case 'd-r':
				$float->setPosition([$arrowMax[0]-1, $arrowMax[1]+1]);
				break;
			case 'd-l':
				$float->setPosition([$arrowMin[0]-1, $arrowMax[1]+1]);
				break;
			}
			return $arrow;
			break;
		case 'u':
			$arrow = self::arrow([$baseMin[0]+1, $baseMin[1]], $len, $direct);
			list($arrowMin, $arrowMax) = $arrow->getRectangle();
			switch ($direct) {
			case 'u':
				$float->setPosition([$arrowMin[0] - 1, $arrowMin[1]-1-($floatMax[1]-$floatMin[1])]);
				break;
			case 'u-r':
				$float->setPosition([$arrowMax[0] - 1, $arrowMin[1]-1-($floatMax[1]-$floatMin[1])]);
				break;
			case 'u-l':
				$float->setPosition([$arrowMin[0] - 1, $arrowMin[1]-1-($floatMax[1]-$floatMin[1])]);
				break;
			}
			return $arrow;
			break;
		}
	}
}
